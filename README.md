#MP3 Service

#NOTE
You make sure your PC have installed NodeJS
if don't have nodejs, let install nodejs through URL : https://nodejs.org/en/download/
#

Installing MP3 Service

1. Frist step install npm via command line : npm install
2. Start server : node app.js

#API
- create nhac : /api/createnhac
- 
Paramater : 
    name : your name music ,
    theloai : category music ,
    hinh : file image ,
    nhac : file music ,

- update nhac : /api/updatenhac/:id
- 
Paramater : 
    name : your name music , 
    theloai : category music ,
    hinh : file image ,
    nhac : file music ,

- delete nhac : /api/updatenhac/:id
- get nhac : /api/getallnhac
- get nhac by id theloai : /api/nhacbytheloaiid/:id

- create theloai : /api/taotheloai
- 
Paramater : 
    tenTL : name category ,
    hinhTL : file image ,
    
- get theloai : /api/getalltheloai

- update theloai : /api/updatetheloai/:id
- 
Paramater : 
    tenTL : name category ,
    hinhTL : file image ,
    
- delete theloai : /api/deletetheloai/:id

- get theloai by id : /api/gettheloaibyid/:id