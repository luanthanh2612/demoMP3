import { MP3Page } from './app.po';

describe('mp3 App', () => {
  let page: MP3Page;

  beforeEach(() => {
    page = new MP3Page();
  });

  it('should display welcome message', done => {
    page.navigateTo();
    page.getParagraphText()
      .then(msg => expect(msg).toEqual('Welcome to app!!'))
      .then(done, done.fail);
  });
});
