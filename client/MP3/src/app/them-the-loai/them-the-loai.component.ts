import { Component, OnInit } from '@angular/core';
import {TheloaiService} from '../services/theloai.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import {Router} from '@angular/router';

@Component({
  selector: 'app-them-the-loai',
  templateUrl: './them-the-loai.component.html',
  styleUrls: ['./them-the-loai.component.css']
})
export class ThemTheLoaiComponent implements OnInit {
  fileIMG : any;
  tenTheLoai : string;
  constructor(private theLoaiService : TheloaiService,private router:Router,private flashMessage : FlashMessagesService) { }

  ngOnInit() {
  }
  fileIMGChange($event){
    this.fileIMG = $event.target.files[0];
  }
  onThem(){
    if(this.tenTheLoai === '' || this.tenTheLoai === undefined){
      this.flashMessage.show('Vui long dien day du thong tin',{cssClass : 'alert-danger'});
    }else if(this.fileIMG === undefined){
      this.flashMessage.show('Vui long chon file',{cssClass : 'alert-danger'});
    }else{
      const theLoai : FormData = new FormData();
      theLoai.append('tenTL',this.tenTheLoai);
      theLoai.append('hinhTL',this.fileIMG);

      this.theLoaiService.themTheLoai(theLoai).subscribe(data =>{
          if(data.success){
            this.flashMessage.show(data.msg,{cssClass : 'alert-success'});
            this.router.navigate(['theloai']);
          }else{
            this.flashMessage.show(data.msg,{cssClass : 'alert-danger'});
          }
      });
    }
  }
}
