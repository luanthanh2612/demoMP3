import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThemTheLoaiComponent } from './them-the-loai.component';

describe('ThemTheLoaiComponent', () => {
  let component: ThemTheLoaiComponent;
  let fixture: ComponentFixture<ThemTheLoaiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThemTheLoaiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThemTheLoaiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
