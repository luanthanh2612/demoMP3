
import { Injectable } from '@angular/core';
import {Http, Headers } from '@angular/http';
import {tokenNotExpired} from 'angular2-jwt';
import 'rxjs/add/operator/map';

@Injectable()
export class AuthService {
  authToken:any;
  user : any;
  URL_HOST:string = 'http://localhost:3000';

  constructor(private http:Http) { }


  login(user:any){
    let header:Headers = new Headers();
    header.append('Content-Type','application/json');
    return this.http.post(this.URL_HOST +'/api/dangnhapuser',user,{headers : header}).map(res=>res.json());
  }

  getProfileUser(){

    this.loadToken();
    let header : Headers = new Headers();
    header.append('Content-Type','application/json');
    header.append('Authorization',this.authToken);

    return this.http.get(this.URL_HOST + '/api/profileuser',{headers : header}).map(res => res.json());
  }
  storageTokenUser(token){
    localStorage.setItem('token',token);
    this.authToken = token;
  }

  loadToken(){
    const token = localStorage.getItem('token');
    this.authToken = token;
  }

  isLogIn(){
    return tokenNotExpired();
  }
  logOut(){
    this.authToken = null;
    localStorage.clear();
  }
}
