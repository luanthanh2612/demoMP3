import { Injectable } from '@angular/core';
import {Http,Headers} from '@angular/http';
import 'rxjs/add/operator/map';
import {AuthService} from './auth.service';

@Injectable()
export class TheloaiService {

  constructor(private http:Http,private auth:AuthService) {
    this.auth.loadToken();
   }

  getAllTheLoai(){
    let headers : Headers = new Headers();
    headers.append('Content-Type','application/json');

    return this.http.get(this.auth.URL_HOST + '/api/getalltheloai',{headers : headers}).map(res => res.json());
  }

  getTheLoaiFromServerById(id){
    let headers : Headers = new Headers();
    headers.append('Content-Type','application/json');
    headers.append('Authorization',this.auth.authToken);
    return this.http.get(this.auth.URL_HOST + '/api/gettheloaibyid/' +id,{headers : headers}).map(res=>res.json());
  }
  
  themTheLoai(theloai){
    let headers : Headers = new Headers();
    headers.append('Authorization',this.auth.authToken);

    return this.http.put(this.auth.URL_HOST + '/api/taotheloai',theloai,{headers : headers}).map(res => res.json());
  }

  deleteTheLoaiById(id){
    let headers : Headers = new Headers();
    headers.append('Authorization',this.auth.authToken);

    return this.http.delete(this.auth.URL_HOST + '/api/deletetheloai/' + id,{headers : headers}).map(res => res.json());

  }

  updateTheLoaiById(id,theloai){
    let headers : Headers = new Headers();
    headers.append('Authorization',this.auth.authToken);

    return this.http.post(this.auth.URL_HOST + '/api/updatetheloai/'+id,theloai,{headers : headers}).map(res => res.json());
  }


}
