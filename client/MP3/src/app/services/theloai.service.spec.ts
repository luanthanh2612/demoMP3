import { TestBed, inject } from '@angular/core/testing';

import { TheloaiService } from './theloai.service';

describe('TheloaiService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TheloaiService]
    });
  });

  it('should be created', inject([TheloaiService], (service: TheloaiService) => {
    expect(service).toBeTruthy();
  }));
});
