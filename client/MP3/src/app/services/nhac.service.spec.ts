import { TestBed, inject } from '@angular/core/testing';

import { NhacService } from './nhac.service';

describe('NhacService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NhacService]
    });
  });

  it('should be created', inject([NhacService], (service: NhacService) => {
    expect(service).toBeTruthy();
  }));
});
