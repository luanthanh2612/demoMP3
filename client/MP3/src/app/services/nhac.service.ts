import { Injectable } from '@angular/core';
import {Http,Headers} from '@angular/http';
import 'rxjs/add/operator/map';
import {AuthService} from './auth.service';

@Injectable()
export class NhacService {

  constructor(private http : Http,private auth:AuthService) {
    this.auth.loadToken();
   }

  getAllNhacFromServer(){

    let headers : Headers = new Headers();
    headers.append('Content-Type','application/json');

    return this.http.get(this.auth.URL_HOST + '/api/getallnhac',{headers : headers}).map(res => res.json());
  }

  addNhacToServer(nhac){
    let headers : Headers = new Headers();
    
    // headers.append('Content-Type','application/x-www-form-urlencoded');
    // headers.append('Accept','application/json');
    headers.append('Authorization',this.auth.authToken);

    return this.http.put(this.auth.URL_HOST + '/api/createnhac',nhac,{headers : headers}).map(res => res.json());
  }
  deleteNhacById(id){
    let headers : Headers = new Headers();
    headers.append('Content-Type','application/json');
    headers.append('Authorization',this.auth.authToken);

    return this.http.delete(this.auth.URL_HOST + '/api/deletenhac/'+id,{headers : headers}).map(res => res.json());
  }

  updateNhacById(id,nhac){
    let headers: Headers = new Headers();
    headers.append('Authorization',this.auth.authToken);

    return this.http.post(this.auth.URL_HOST + '/api/updatenhac/'+id,nhac,{headers : headers}).map(res => res.json());
  }

}
