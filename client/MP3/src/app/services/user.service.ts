
import { Injectable } from '@angular/core';
import {Http,Headers} from '@angular/http';
import 'rxjs/add/operator/map';
import {AuthService} from './auth.service';

@Injectable()
export class UserService {

  constructor(private http:Http,private authService:AuthService) { 

    this.authService.loadToken();
  }

  getAllUserFormServer(){
   
    let header: Headers = new Headers();
    header.append('Content-Type','application/json');
    header.append('Authorization',this.authService.authToken);

    return this.http.get(this.authService.URL_HOST + '/api/getalluser',{headers : header}).map(res => res.json());
  }

  deleteUserFromServerById(id){
    let header:Headers = new Headers();
    header.append('Content-Type','application/json');
    header.append('Authorization',this.authService.authToken);

    return this.http.delete(this.authService.URL_HOST+'/api/deleteuser/'+id,{headers : header}).map(res=> res.json());
  }

  getUserFromServerById(id){
    let header:Headers = new Headers();
    header.append('Content-Type','application/json');
    header.append('Authorization',this.authService.authToken);

    return this.http.get(this.authService.URL_HOST + '/api/getuser/' + id,{headers : header}).map(res => res.json());
  }

  updateUserById(id,user){
    let headers : Headers = new Headers();
    headers.append('Content-Type','application/json');
    headers.append('Authorization',this.authService.authToken);

    return this.http.post(this.authService.URL_HOST + '/api/updateuser/' + id,user,{headers : headers}).map(res => res.json());
  }

}
