import { Injectable } from '@angular/core';
import {Http,Headers} from '@angular/http';
import 'rxjs/add/operator/map';
import {AuthService} from './auth.service';

@Injectable()
export class DeviceService {

  constructor(private authService : AuthService,private http:Http) {
    authService.loadToken();
   }

   getListDevice(){
     let header : Headers = new Headers();
     header.append('Authorization',this.authService.authToken);

     return this.http.get(this.authService.URL_HOST + "/api/devices",{headers : header}).map(res => res.json());
   }

   deleteDevice(id){
     let header : Headers = new Headers();
     header.append('Authorization',this.authService.authToken);

    return this.http.delete(this.authService.URL_HOST + "/api/deletedevice/" + id,{headers : header}).map(res =>res.json());
   }

   sendMessage(message){
     let header : Headers = new Headers();
     header.append('Authorization',this.authService.authToken);

     return this.http.post(this.authService.URL_HOST + "/api/send",message,{headers : header}).map(res => res.json());

   }

}
