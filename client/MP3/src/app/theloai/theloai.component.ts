import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TheloaiService } from '../services/theloai.service';
import { FlashMessagesService } from 'angular2-flash-messages';
@Component({
  selector: 'app-theloai',
  templateUrl: './theloai.component.html',
  styleUrls: ['./theloai.component.css']
})
export class TheloaiComponent implements OnInit {
  listTheLoai : any;
  constructor(private router:Router,private theloaiService : TheloaiService,private flashMessage : FlashMessagesService) { }

  ngOnInit() {
    this.theloaiService.getAllTheLoai().subscribe(rs => {
        this.listTheLoai = rs.listTheLoai;
    });

  }

  onThem(){
    this.router.navigate(['themtheloai']);
  }
  onDelete(id){
   this.theloaiService.deleteTheLoaiById(id).subscribe(rs => {
      if(rs.success){
        this.flashMessage.show(rs.msg,{cssClass : 'alert-success'});
        window.location.reload();
      }else{
        this.flashMessage.show(rs.msg,{cssClass : 'alert-danger'});
      }
   });
  }
  onUpdate(id){
    this.router.navigate(['updatetheloai',id]);
  }
}
