import { timeout } from 'rxjs/operator/timeout';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FlashMessagesService } from 'angular2-flash-messages';
import {AuthService} from '../services/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  
  username : string;
  password : string;

  constructor(private flashMessageService : FlashMessagesService,private authService : AuthService,private router:Router) { }

  ngOnInit() {
    
  }

  Login(){
    if(this.username === '' || this.username === undefined || this.password === '' || this.password === undefined){
      this.flashMessageService.show('Vui long nhap day du',{timeout : 3000,cssClass:'alert-danger'});
    }else{
      let user = {
        username : this.username,
        password : this.password
      }

     this.authService.login(user).subscribe(data => {
       if(!data.success){
         this.flashMessageService.show(data.msg,{cssClass : 'alert-danger'});
       }else{
         let token = data.token;
         this.authService.storageTokenUser(token);

         this.authService.getProfileUser().subscribe(profile => {
           if(profile.user.rule === 1){
            localStorage.setItem('name',profile.user.name);
            this.router.navigate(['/user']);
            window.location.reload();
           }else{
             this.flashMessageService.show('Khong duoc phep',{cssClass : 'alert-danger'});
           }
         });
       }
     });
    }
  }

}
