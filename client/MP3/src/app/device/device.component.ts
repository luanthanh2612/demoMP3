import { Component, OnInit } from '@angular/core';
import { DeviceService} from '../services/device.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import * as io from 'socket.io-client';

@Component({
  selector: 'app-device',
  templateUrl: './device.component.html',
  styleUrls: ['./device.component.css']
})
export class DeviceComponent implements OnInit {

  checked : boolean;
  listDevice : any;
  registerId: string;
  title : string;
  content:string;

  constructor(private serviceDevice : DeviceService,private flashMessage : FlashMessagesService) { }
  socket = io('http://localhost:2000');
  ngOnInit() {
   
    this.socket.on('update',(data)=>{
      console.log(data);
      if(data.update){

          this.flashMessage.show(data.message,{cssClass : 'alert-success'});
          this.serviceDevice.getListDevice().subscribe((rs)=>{
             this.listDevice = rs.devices;
          });
      }

    });

    this.serviceDevice.getListDevice().subscribe((rs)=>{
      this.listDevice = rs.devices;
   });

  }
  onDelete(id,i){
    this.serviceDevice.deleteDevice(id).subscribe((rs)=>{
        if(rs.success){
          this.socket.emit('update',{message : "da xoa thanh cong"});
          this.listDevice.splice(i,1);
        }
    });
  }

  toggle(registerId){
    this.registerId = registerId;
  }

  notifi(){
    if(this.checked){
      if(!this.registerId === undefined || this.registerId != ''){
        if(this.title === undefined || this.title === '' || this.content === undefined || this.content === ''){
          this.flashMessage.show('vui long nhap',{cssClass : 'alert-danger'});
        }else{
            let message = {
                registerId : this.registerId,
                message : this.content,
                title : this.title
            }
            this.serviceDevice.sendMessage(message).subscribe(rs => console.log(rs));
        }
      }
    }else{
      this.flashMessage.show("Moi chon thiet bi",{cssClass : 'alert-danger'});
    }
  } 
 
}
