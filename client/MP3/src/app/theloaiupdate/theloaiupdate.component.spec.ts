import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TheloaiupdateComponent } from './theloaiupdate.component';

describe('TheloaiupdateComponent', () => {
  let component: TheloaiupdateComponent;
  let fixture: ComponentFixture<TheloaiupdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TheloaiupdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TheloaiupdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
