import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute, Params } from "@angular/router";
import { TheloaiService } from "../services/theloai.service";
import { FlashMessagesService } from "angular2-flash-messages";

@Component({
  selector: "app-theloaiupdate",
  templateUrl: "./theloaiupdate.component.html",
  styleUrls: ["./theloaiupdate.component.css"]
})
export class TheloaiupdateComponent implements OnInit {
  tenTheLoai: any;
  fileIMG: any;
  paramID: any;

  constructor(
    private theLoaiService: TheloaiService,
    private router: Router,
    private activatedRouter: ActivatedRoute,
    private flashMessage: FlashMessagesService
  ) {}

  ngOnInit() {
    this.activatedRouter.params.subscribe((params: Params) => {
      this.paramID = params["id"];
    });
  }
  fileIMGChange($event) {
    this.fileIMG = $event.target.files[0];
  }
  onUpdate() {
    if (this.tenTheLoai === "" || this.tenTheLoai === undefined) {
      this.flashMessage.show("Vui long nhap day du", {
        cssClass: "alert-danger"
      });
    } else if (this.fileIMG === undefined) {
      this.flashMessage.show("Vui long nhap file", {
        cssClass: "alert-danger"
      });
    } else {
      let theLoai: FormData = new FormData();
      theLoai.append("tenTL", this.tenTheLoai);
      theLoai.append("hinhTL", this.fileIMG);

      this.theLoaiService
        .updateTheLoaiById(this.paramID, theLoai)
        .subscribe(result => {
          if (result.success) {
            this.flashMessage.show(result.msg, { cssClass: "alert-success" });
            this.router.navigate(['theloai']);
          } else {
            this.flashMessage.show(result.msg, { cssClass: "alert-danger" });
          }
        });
    }
  }
}
