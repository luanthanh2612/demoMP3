import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NhacupdateComponent } from './nhacupdate.component';

describe('NhacupdateComponent', () => {
  let component: NhacupdateComponent;
  let fixture: ComponentFixture<NhacupdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NhacupdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NhacupdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
