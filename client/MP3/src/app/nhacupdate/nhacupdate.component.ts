import { Component, OnInit } from "@angular/core";
import { TheloaiService } from "../services/theloai.service";
import { NhacService } from "../services/nhac.service";
import { FlashMessagesService } from "angular2-flash-messages";
import { Router, ActivatedRoute, Params } from "@angular/router";

@Component({
  selector: "app-nhacupdate",
  templateUrl: "./nhacupdate.component.html",
  styleUrls: ["./nhacupdate.component.css"]
})
export class NhacupdateComponent implements OnInit {
  listTheLoai: any;
  name: string;
  fileMP3: any;
  fileIMG: any;
  paramId: string;

  constructor(
    private router: Router,
    private nhacService: NhacService,
    private flashMessage: FlashMessagesService,
    private theloaiService: TheloaiService,
    private activatedRouter: ActivatedRoute
  ) {}

  ngOnInit() {
    this.theloaiService.getAllTheLoai().subscribe(theloais => {
      this.listTheLoai = theloais.listTheLoai;
    });

    this.activatedRouter.params.subscribe((params: Params) => {
      this.paramId = params["id"];
    });
  }
  fileMP3Change($event) {
    this.fileMP3 = $event.target.files[0];
  }
  fileIMGChange($event) {
    this.fileIMG = $event.target.files[0];
  }
  onUpdate(idTheLoai) {
    if (this.name === "" || this.name === undefined) {
      this.flashMessage.show("Vui long nhap day du", {
        cssClass: "alert-danger"
      });
    } else if (this.fileIMG === undefined || this.fileMP3 === undefined) {
      this.flashMessage.show("Vui long nhap file", {
        cssClass: "alert-danger"
      });
    } else {
      let nhac: FormData = new FormData();
      nhac.append("name", this.name);
      nhac.append("theloai", idTheLoai);
      nhac.append("hinh", this.fileIMG);
      nhac.append("nhac", this.fileMP3);

      this.nhacService.updateNhacById(this.paramId, nhac).subscribe(resutl => {
        if (resutl.success) {
          this.router.navigate(["nhac"]);
        } else {
          this.flashMessage.show(resutl.msg, { cssClass: "alert-danger" });
        }
      });
    }
  }
}
