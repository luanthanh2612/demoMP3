
import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../app/services/auth.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Route, Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  name : string;
  constructor(private authService : AuthService,private flashMessageService : FlashMessagesService,private router:Router) { }

  ngOnInit() {
    if(this.authService.isLogIn()){
      this.name = localStorage.getItem('name');
    }
    
  }

  onLogOut(){
    this.router.navigate(['/login']);
    this.authService.logOut();
    this.flashMessageService.show('Da dang xuat',{cssClass : 'alert-success'});
    
  }
}
