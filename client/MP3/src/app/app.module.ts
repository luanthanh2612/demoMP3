import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { FormsModule } from "@angular/forms";
import { FlashMessagesModule } from "angular2-flash-messages";
import { HttpModule } from "@angular/http";
import { MaterialModule, MdDialogModule } from "@angular/material";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

import { AppComponent } from "./app.component";
import { LoginComponent } from "./login/login.component";
import { NavbarComponent } from "./navbar/navbar.component";
import { AuthService } from "../app/services/auth.service";
import { UserComponent, DialogOverView } from "./user/user.component";
import { AuthGuard } from "../app/guards/auth.guard";
import { NhacComponent } from "./nhac/nhac.component";
import { UserService } from "../app/services/user.service";
import { UpdateComponent } from "./update/update.component";
import { NotfoundComponent } from "./notfound/notfound.component";
import { HomeComponent } from "./home/home.component";
import {NhacService} from '../app/services/nhac.service';
import { ThemComponent } from './them/them.component';
import {TheloaiService} from '../app/services/theloai.service';
import { TheloaiComponent } from './theloai/theloai.component';
import { ThemTheLoaiComponent } from './them-the-loai/them-the-loai.component';
import { NhacupdateComponent } from './nhacupdate/nhacupdate.component';
import { TheloaiupdateComponent } from './theloaiupdate/theloaiupdate.component';
import { DeviceComponent } from './device/device.component';
import {DeviceService} from '../app/services/device.service';

const appRoutes: Routes = [
  {
    path : 'home',
    component : HomeComponent
  },
  {
    path: "login",
    component: LoginComponent
  },
  {
    path: "user",
    component: UserComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "nhac",
    component: NhacComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "update/:id",
    component: UpdateComponent,
    canActivate: [AuthGuard]
  },
  {
    path : "them",
    component : ThemComponent,
    canActivate : [AuthGuard]
  },
  {
    path : 'theloai',
    component : TheloaiComponent,
    canActivate : [AuthGuard]
  },
  {
    path : 'themtheloai',
    component : ThemTheLoaiComponent,
    canActivate : [AuthGuard]
  },
  {
    path : 'updatenhac/:id',
    component : NhacupdateComponent,
    canActivate : [AuthGuard]
  },
  {
    path : 'updatetheloai/:id',
    component : TheloaiupdateComponent,
    canActivate : [AuthGuard]
  },
  {
    path : 'device',
    component : DeviceComponent,
    canActivate : [AuthGuard]
  },
  {
    path: "**",
    component: NotfoundComponent
  }
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    NavbarComponent,
    UserComponent,
    NhacComponent,
    DialogOverView,
    UpdateComponent,
    NotfoundComponent,
    HomeComponent,
    ThemComponent,
    TheloaiComponent,
    ThemTheLoaiComponent,
    NhacupdateComponent,
    TheloaiupdateComponent,
    DeviceComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    FlashMessagesModule,
    HttpModule,
    RouterModule.forRoot(appRoutes),
    MaterialModule,
    MdDialogModule,
    BrowserAnimationsModule
  ],
  entryComponents: [DialogOverView],
  providers: [AuthService, AuthGuard, UserService,NhacService,TheloaiService,DeviceService],
  bootstrap: [AppComponent]
})
export class AppModule {}
