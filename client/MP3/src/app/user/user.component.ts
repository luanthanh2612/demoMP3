import { User } from '../../user';
import { Component, OnInit } from '@angular/core';
import {UserService} from '../services/user.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import {MdDialog, MdDialogRef, MD_DIALOG_DATA} from '@angular/material';
import { Route, Router } from '@angular/router';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  listUser : Array<User>;
  constructor(private userService:UserService,private flashMessageService:FlashMessagesService,private router : Router) { }

  ngOnInit() {
    this.userService.getAllUserFormServer().subscribe(listUser => this.listUser = listUser.list);
  }
  
  onDelete(id){
    this.userService.deleteUserFromServerById(id).subscribe(data=>{
      if(data.success){
        window.location.reload();
      }
    });
  }
  onUpdate(id){
    this.router.navigate(['/update',id]);
  }
}
@Component({
  selector : 'dialog-overview',
  templateUrl : './dialog.component.html'
})
export class DialogOverView{
  constructor(){}

  
}
