import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NhacComponent } from './nhac.component';

describe('NhacComponent', () => {
  let component: NhacComponent;
  let fixture: ComponentFixture<NhacComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NhacComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NhacComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
