import { Component, OnInit } from '@angular/core';
import { NhacService } from '../services/nhac.service';
import {TheloaiService} from '../services/theloai.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import {Nhac} from '../../nhac';

import {Router} from '@angular/router';

@Component({
  selector: 'app-nhac',
  templateUrl: './nhac.component.html',
  styleUrls: ['./nhac.component.css']
})
export class NhacComponent implements OnInit {
  listNhac : Array<Nhac>;
  listTL:any ;
  constructor(private nhacService:NhacService,private router:Router,private theLoaiService:TheloaiService,private flashMessage:FlashMessagesService) { }

  ngOnInit() {
    this.nhacService.getAllNhacFromServer().subscribe(data=>{
      this.listNhac = data.nhacs;
    });

    this.theLoaiService.getAllTheLoai().subscribe(rs => {
       this.listTL= rs.listTheLoai;
    });

  }
  onThem(){
    this.router.navigate(['them']);
  }
  onDelete(id){
    this.nhacService.deleteNhacById(id).subscribe(rs => {
        if(rs.success){
          this.flashMessage.show(rs.msg,{cssClass : 'alert-success'});
          window.location.reload();
        }else{
          this.flashMessage.show(rs.msg,{cssClass : 'alert-danger'});
        }
    });
  }
  onUpdate(id){
    this.router.navigate(['/updatenhac',id]);
  }
}
