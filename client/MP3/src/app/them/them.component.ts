import { Component, OnInit } from '@angular/core';
import {TheloaiService} from '../services/theloai.service';
import {NhacService} from '../services/nhac.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import {Router} from '@angular/router';

@Component({
  selector: 'app-them',
  templateUrl: './them.component.html',
  styleUrls: ['./them.component.css']
})
export class ThemComponent implements OnInit {
  listTheLoai : any;
  name : string;
  fileMP3 : any;
  fileIMG : any;

  constructor(private theloaiService:TheloaiService,private nhacService : NhacService, private flashMessage:FlashMessagesService,private router:Router) { }

  ngOnInit() {

      this.theloaiService.getAllTheLoai().subscribe(data=>{this.listTheLoai = data.listTheLoai});

  }

  fileMP3Change($event){
    this.fileMP3 = $event.target.files[0];
  }

  fileIMGChange($event){
    this.fileIMG = $event.target.files[0];
  }

  onThem(id){
    if(this.name === '' || this.name === undefined){
      this.flashMessage.show('Vui long dien day du thong tin',{cssClass: 'alert-danger'});
    }else if(this.fileIMG === undefined || this.fileMP3 === undefined){
        this.flashMessage.show('Vui long chon file',{cssClass : 'alert-danger'});
    }
    else{
      const nhac : FormData = new FormData();
      nhac.append('name',this.name);
      nhac.append('theloai',id);
      nhac.append('hinh',this.fileIMG);
      nhac.append('nhac',this.fileMP3);
  
      this.nhacService.addNhacToServer(nhac).subscribe(data => {
        console.log(data);
          if(data.success){
            this.flashMessage.show(data.msg,{cssClass : 'alert-success'});
            this.router.navigate(['nhac']);
          }else{
            this.flashMessage.show(data.msg,{cssClass : 'alert-danger'});
          }
  
      });
    }
  }

}
