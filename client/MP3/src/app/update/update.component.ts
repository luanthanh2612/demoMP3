import { Component, OnInit } from '@angular/core';
import {UserService} from '../services/user.service';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css']
})
export class UpdateComponent implements OnInit {
  
  username : string;
  name : string;
  rule : number;
  userId : any;

  constructor(private router:Router ,private userService : UserService,private activatedRouter : ActivatedRoute,private flashMessage : FlashMessagesService) { }

  ngOnInit() {
    this.activatedRouter.params.subscribe((params : Params)=>{
        this.userId = params['id'];
        this.userService.getUserFromServerById(this.userId).subscribe(user => {
          
          this.username = user.username;
          this.name = user.name;
          this.rule = user.rule;

        });
    });
   
   
  }

  updateUser(){
   let user = {
     username : this.username,
     name : this.name,
     rule : this.rule
   }

   this.userService.updateUserById(this.userId,user).subscribe(result => {
     if(result.success){
        this.flashMessage.show(result.msg,{cssClass : 'alert-success'});
        this.router.navigate(['user']);
     }
   });

  }

}
