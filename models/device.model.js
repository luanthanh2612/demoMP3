const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const deviceSchema = new Schema({

    deviceId: {
        type : String,
        required : true
    },
    deviceName : {
        type: String,
        required : true
    },
    registerId : {
        type : String,
        required : true
    }

},{collection : 'device'});

const deviceModel = module.exports = mongoose.model('device',deviceSchema);

module.exports.addDevice = function(newDevice,callback){
    newDevice.save(callback);
}

module.exports.getListDevice = function(callback){
    deviceModel.find({},{_id : false,__v:false},callback);
}

module.exports.deleteDeviceByDeviceId = function(deviceId,callback){
    deviceModel.findOneAndRemove({deviceId : deviceId},callback);
}

module.exports.getDeviceByRegisterId = function(regId,callback){
    deviceModel.findOne({registerId : regId},callback);
}

