const route = require("express").Router();
const sendMessage = require("../config/sendNotification");
const deviceModel = require("../models/device.model");
const passport = require("passport");
const config = require("../config/config");
const io = require("socket.io");

let socket = io.listen(2000);
socket.on("connection", sk => {
  console.log("Client Connected");

  sk.on("update", msg => {
    console.log(msg);
  });
});

route.get('/devices',passport.authenticate('jwt',{session : false}),(req,res)=>{
    deviceModel.getListDevice((err,rs)=>res.json({devices : rs}));
});

route.post("/device", (req, res) => {
   
    if(typeof req.body.deviceId === undefined || typeof req.body.deviceName === undefined || typeof req.body.registerId === undefined){
        res.json({success : false, msg : 'undefine'});
    }else{
        deviceModel.getDeviceByRegisterId(req.body.registerId, (err, device) => {
            if (!device) {
                let newDeviceModel = new deviceModel({
                    deviceId: req.body.deviceId,
                    deviceName: req.body.deviceName,
                    registerId: req.body.registerId
                  });
            
                  deviceModel.addDevice(newDeviceModel, (err, rs) => {
                    if (err) throw err;
                    socket.emit('update',{message : 'Them Thiet bi moi',update : true});
                  });
            } 
            res.json({success : false,msg : config.MESSAGE_THELOAI.THELOAI_THAT_BAI});
            
          });
    }
});

route.post('/send',passport.authenticate('jwt',{session : false}),(req,res)=>{
    let registerId = req.body.registerId;
    let message = req.body.message;
    let title = req.body.title;
   sendMessage.sendNotifycation(message,title,registerId);
});

route.delete('/deletedevice/:id',passport.authenticate('jwt',{session : false}),(req,res)=>{
    let deviceId = req.params.id;
    deviceModel.deleteDeviceByDeviceId(deviceId,(err,rs)=>{
        if(err) throw err;
        res.json({success : true,msg : config.MESSAGE_THELOAI.THELOAI_THANH_CONG});
    });
});


module.exports = route;
