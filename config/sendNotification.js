const gcm = require('node-gcm');
const config = require('./config');

exports.sendNotifycation = function(message,title,registrationId){

    let message1 = new gcm.Message(
        {
        priority: 'high',
        contentAvailable: true,
        // delayWhileIdle: true,
        // dryRun: true,
        data : {message : message},
        notification : {title : title ,body : message}
    });
    let regToken = [registrationId];

    let sender = new gcm.Sender(config.GCM.gcm_api_key);
    sender.send(message1,{registrationTokens : regToken},(err,res)=>{
        
        if(err){
            console.log(err);
            
        }else{
            console.log(res);
        }

    });


}
